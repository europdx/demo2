#!/usr/bin/env python3

import requests
import json

import sys
import time

hub = 'https://datahub-dev.edirex.ics.muni.cz/api/v1'

r = requests.get(hub + '/tmplists/' + sys.argv[1] + '/expressions')

if r.status_code == 201:
	j = r.json()
	print(j)
else:
	print('Status: ', r.status_code)
	print(r.text)
	sys.exit(1)

