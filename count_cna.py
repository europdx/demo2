#!/usr/bin/env python3

import json
import sys

with open(sys.argv[1]) as fin:
	j = json.load(fin)


cnt = {}

for k in j:
	id = k['pdxmodel_id']
#	print(id)
	for c in k['cnas']:
		if c['value_of'] != 0.0:
			if id in cnt:
				cnt[id] += 1
			else:
				cnt[id] = 1


for k in sorted(cnt.keys(), key=lambda x: cnt[x]):
	print(k, cnt[k])
