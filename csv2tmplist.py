#!/usr/bin/env python3

import requests
import json

import sys
import time

hub = 'https://datahub-dev.edirex.ics.muni.cz/api/v1'

ids = []
first = True

with open(sys.argv[1]) as fin:
	for line in fin:
		f = line.split(',')
		if not first: ids.append(f[2])
		first = False
		
print('ID list: ',ids)

tmplist_req = json.dumps({'pdxmodel_list' : ids})

print('/tmplists request', tmplist_req)

r = requests.post(hub + '/tmplists',data = tmplist_req,headers={'Content-Type':'application/json'})

if r.status_code == 201:
	listid = r.json()['tmplistid']
	print('List id: ',listid)
else:
	print('Status: ', r.status_code)
	print(r.text)
	sys.exit(1)
