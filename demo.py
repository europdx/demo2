#!/usr/bin/env python3

import requests
import json

import sys
import time

hub = 'https://datahub-dev.edirex.ics.muni.cz/api/v1'
cbod = 'http://cbiood.edirex.ics.muni.cz'
cbod_api = cbod + '/api/v1'

ids = []
first = True

with open(sys.argv[1]) as fin:
	for line in fin:
		f = line.split(',')
		if not first: ids.append(f[2])
		first = False
		
print('ID list: ',ids)

tmplist_req = json.dumps({'pdxmodel_list' : ids})

print('/tmplists request', tmplist_req)

r = requests.post(hub + '/tmplists',data = tmplist_req,headers={'Content-Type':'application/json'})

if r.status_code == 201:
	listid = r.json()['tmplistid']
	print('List id: ',listid)
else:
	print('Status: ', r.status_code)
	print(r.text)
	sys.exit(1)

cbod_req = json.dumps({'user': { 'userId' : 'dummy'}, 'sampleIDs': str(listid) })
print('/cbioondemand request:', cbod_req)

r = requests.post(cbod_api + '/cbioondemand', data = cbod_req, headers={'Content-Type':'application/json'}, verify=False)

print(r)

if r.status_code == 200:
	cbod_id = r.json()['id']
else:
	print('Status: ',r.status_code)
	print(r.text)
	sys.exit(1)

# {"id":"cod8wq77","user":{"userId":null},"secondsToExpire":600000,"url":"7f25f5a3-58c2-432b-9662-df1defc031f3","status":"creating"}

r_json = r.json()
print('Response:', r_json)


stat_req = cbod_api + '/cbioondemand?id=%s&user.userId=%s' % (r_json['id'], r_json['user']['userId'])
print(stat_req)

while True:
	stat = requests.get(stat_req,verify=False)
	if stat.status_code == 200:
		if stat.json()['status'] == 'active':
			break
		else:
			print('Status: ',stat.json()['status'])
	time.sleep(5)

print('cBioPortal ready at %s/%s' % (cbod,r_json['url']))

