#!/usr/bin/env python3

import requests
import json

import sys
import time

cbod = 'http://cbiood.edirex.ics.muni.cz/api/v1'

user = 'dummy'
id = sys.argv[1]

req = cbod + '/cbioondemand?id=%s&user.userId=%s' % (id, user)
print(req)

while True:
	stat = requests.get(req, verify = False)
	print(stat.status_code)
	print(stat.text)
	time.sleep(5)
