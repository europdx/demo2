#!/usr/bin/env python3

import requests
import json

import sys
import time

hub = 'https://datahub-dev.edirex.ics.muni.cz/api/v1'
cbod = 'http://cbiood.edirex.ics.muni.cz'
cbod_api = cbod + '/api/v1'

ids = []

with open(sys.argv[1]) as fin:
	for l in fin:
		ids.append(l[:-1])

print('ID list: ',ids)

tmplist_req = json.dumps({'pdxmodel_list' : ids})

print('/tmplists request', tmplist_req)

r = requests.post(hub + '/tmplists',data = tmplist_req,headers={'Content-Type':'application/json'})

if r.status_code == 201:
	listid = r.json()['tmplistid']
	print('List id: ',listid)
else:
	print('Status: ', r.status_code)
	print(r.text)
	sys.exit(1)
