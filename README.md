# EurOPDX DataPortal demo @ general assembly meeting

Základní Scénář
- začít na playground, okomentovat nový design
- překlik na search
  - může být provizorní, tj. stará verze dataporatal (máme stejná data)
- výběr reprezentativní sady
  - věk 60-69, dává 225 výsledků
  - stáhnout csv
- výroba tmplistu, zpětný dotaz na něj
  - skript csv2tmplist.py
  - curl -X GET https://datahub-dev.edirex.ics.muni.cz/api/v1/tmplists/169/pdxmodels
  - dtto cnas, expressions
- cbio on demand, status clusteru 
  - watch kubectl --insecure-skip-tls-verify -n cbio-on-demand get all
- pustit v jednom okně start cBP (cbod-create.py), v druhém sledovat, co se děje
- podříznout černého kohouta a cBP naběhne, na Macu pustit v Chrome


